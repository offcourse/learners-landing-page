(ns app.components.menubar)

(defn menubar [site-title]
  [:div.ui.inverted.segment
   [:div.ui.container
    [:div.ui.inverted.segment
     [:div.ui.container
      [:h1 site-title]]]]])
