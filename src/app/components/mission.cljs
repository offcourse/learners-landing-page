(ns app.components.mission)

(defn section [{:keys [title content]}]
  ^{:key "mission"} [:div.ui.center.aligned.very.padded.segment
                     [:h1.ui.header title]
                     [:p content]])
