(ns app.components.main
  (:require [app.templates.layout :refer [layout]]
            [app.components.what :as what]
            [app.components.why :as why]
            [app.components.how :as how]
            [app.components.mission :as mission]))

(defn page [{:keys [site-title what why how mission]}]
  (layout site-title
          [:div.ui.basic.segments
           (what/section what)
           (why/section why)
           (how/section how)
           (mission/section mission)]))
