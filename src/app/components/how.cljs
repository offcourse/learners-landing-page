(ns app.components.how)

(defn section [{:keys [video-data slogan]}]
  ^{:key "how"}[:div.ui.segment
                [:div.ui.embed video-data]
                [:p slogan]])
