(ns app.components.why)

(defn activity [{:keys [title icon slogan]}]
  ^{:key title} [:div.ui.center.aligned.segment
                 [:h2.ui.icon.header
                  [:i.circular.icon {:class icon}]
                  title]
                 [:p slogan]])

(defn section [{:keys [title activities]}]
  {:key "why"} [:div.ui.segment
                [:h1.ui.centered.header title]
                [:div.ui.center.aligned.segment
                 [:div.ui.horizontal.segments
                  (map activity activities)]]])
