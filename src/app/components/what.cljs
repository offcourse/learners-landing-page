(ns app.components.what)

(defn section [{:keys [link image slogan]}]
  ^{:key "what"} [:div.ui.padded.segment
                     [:a.ui.fluid.image {:href link}
                      [:img {:src image}]]
                     [:h1.ui.small.header slogan]])
