(ns app.parser
  (:require [cljs.nodejs :as node]))

(def program (node/require "commander"))

(defn args [options]
  (doseq [[flags operation extra] (vals options)]
    (.option program flags operation extra))
  (.parse program (.-argv js/process))
  (into {} (map (fn [arg] [arg (aget program (name arg))]) (keys options))))
