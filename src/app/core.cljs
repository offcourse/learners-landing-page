(ns app.core
  (:require [cljs.nodejs :as node]
            [app.parser :as parse]
            [app.components.main :refer [page]]
            [reagent.core :as reagent]
            [clojure.string :as str]))

(node/enable-util-print!)

(def fs (node/require "fs"))
(def yaml (node/require "js-yaml"))

(defn ^:export render-page [page data]
  (reagent/render-to-static-markup [page data]))

(defn to-json [data]
  (.stringify js/JSON (clj->js data)))

(defn to-yaml [data]
  (.safeDump yaml (clj->js data)))

(defn from-yaml [data]
  (.safeLoad yaml data))

(defn load [file-name]
  (-> (.readFileSync fs file-name)
      (.toString "utf-8")
      (from-yaml)
      (js->clj :keywordize-keys true)))

(defn save [file-name dir-name content]
  (let [dir-path (str "dist/" dir-name "/")
        file-path (str dir-path file-name)]
    (if (not (.existsSync fs dir-path))
      (do
        (.mkdirSync fs dir-path)
        (.writeFileSync fs file-path content))
      (.writeFileSync fs file-path content))))

(defn to-dirname [path]
  (-> path
      (str/split "/")
      second
      (str/split ".")
      first))

(defn -main []
  (let [options {:input  ["-i, --input [file]" "input [file]" "file"]}
        {:keys [input]} (parse/args options)
        data      (load input)
        dir-name (to-dirname input)]
    (save "data.json" dir-name (to-json data))
    (save "index.html"  dir-name (render-page page data))))

(set! *main-cli-fn*
      -main)
