(ns app.templates.layout
  (:require [app.components.menubar :refer [menubar]]))

(defn layout [site-title main]
  [:html
   [:head
    [:meta {:charset "utf-8"}]
    [:meta {:name    "viewport"
            :content "width=device-width, initial-scale=1.0"}]
    [:link {:rel "stylesheet" :type "text/css" :href "semantic.min.css"}]]
   [:body
    (menubar site-title)
    [:div.ui.container main]]])
